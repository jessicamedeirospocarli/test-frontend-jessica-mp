# Configurar o Projeto

1. Na raiz do projeto execute o comando a baixo para inicializar o backend
```
json-server --watch db.json --port 3001
```

2.  Na raiz do projeto execute o comando a baixo para inicializar o frontend
```
npm start
```


# Akna Software - Analista Programador FrontEnd

## Objetivo

Desenvolva um projeto básico que consiste em um site de uma pequena livraria. As funcionalidades deste site serão mais focadas em um simples CRUD (CREATE, READ, UPDATE, DELETE) de livros, onde é possível cadastrar, listar e remover livros do estoque.

## Instruções de pubicação

1. Para publicar seu projeto você deverá fazer um fork desse repositório.
2. A partir do momento que você criou o fork do repositório você tem até 24h para criar um pullrequest

## Detalhes do projeto

**O projeto consiste na criação de duas telas sendo:**

- Tela de listagem de livros
- Tela de cadastro/edição de livros

### Descrição das telas
__Listagem de livros__

Na tela de listagem de livros deverá haver uma tabela de livros com as colunas de Nome, Autor, Preço e Opções. Na coluna de opções deverá haver um dropdown com as seguintes opções:

- Editar (edita todos os dados do livro)
- Excluir (oferece a opção de excluir o livro)

__Cadastro e Edição de Livros__

Na tela de cadastro/edição de livros haverá um formulário para o preenchimento dos seguintes dados:
- Nome do livro
- Autor do livro
- Preço do livro

Obs: todos os campos deverão ser obrigatórios, e o campo de preço não deverá aceitar letras e nem caracteres especiais.

### Layout
O site deve ser inteiramente responsivo, ou seja, deve suportar em dispositivos como smartphones, tablets, e desktop.

## Tecnologias que podem ser utilizadas

Poderá ser usado um framework ou uma biblioteca em Javascript para desenvolver o projeto, como por exemplo: Angular JS (1, 2, 4, etc...), Vue JS, React JS, JQuery.
Em relação ao CSS, poderá ser usado algum framework como Bootstrap, Materialize CSS, Semantic UI entre outros, fica ao seu critério, ou caso prefira poderá ser usado CSS puro também.
O uso de pré-processadores CSS como SASS ou LESS é opcional.

## API

O desenvolvimento da API deste projeto deverá ser realizado utilizando a biblioteca json-server,que irá ajudá-lo a desenvolver uma API em JSON rapidamente e sem prévio conhecimento em back-end. Essa biblioteca funciona criando uma API fake com o npm possibilitando criar um CRUD rapidamente e salvando os dados em um arquivo JSON.
Nos links a seguir há um breve tutorial (em inglês) explicando o modo de instalação e uso da biblioteca.

Documentação: https://github.com/typicode/json-server#getting-started

Video Tutorial: https://www.youtube.com/watch?v=1zkgdLZEdwM

## Dúvidas

Em caso de duvidas, por favor mande um email para dep.desenvolvimento@akna.com. 



